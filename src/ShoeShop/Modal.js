import React, { Component } from 'react'

export default class Modal extends Component {
    render() {
        return (
            <div className="row pt-5 d-flex align-items-center">
                <div className="col-8 " >
                    <p style={{ fontSize: '26px', fontWeight: 'bold' }}>{this.props.body.name}</p>
                    <p>{this.props.body.price}</p>
                    <p>{this.props.body.description}</p>
                </div>
                <img src={this.props.body.image} alt="" className='col-4' />
            </div>

        )
    }
}
