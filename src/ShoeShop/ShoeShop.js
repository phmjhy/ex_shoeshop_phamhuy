import React, { Component } from 'react'
import { dataShoe } from './dataShoe';
import Cart from './Cart';
import ProductList from './ProductList';
import Modal from './Modal';

export default class ShoeShop extends Component {
    state = {
        list: dataShoe,
        cart: [],
        productDetail: {},
    }

    addToCart = (productItem) => {
        let cloneCart = [...this.state.cart];
        let index = this.state.cart.findIndex((item) => {
            return item.id === productItem.id
        });
        if (index === -1) {
            let cartItem = { ...productItem, cartQty: 1 };
            cloneCart.push(cartItem)
        } else {
            cloneCart[index].cartQty++
        }
        this.setState({ cart: cloneCart })
    }

    updateCart = (index, adjust) => {
        let cloneCart = [...this.state.cart]
        if (adjust === "INCREASE") {
            cloneCart[index].cartQty++
        }
        if (adjust === "DECREASE") {
            cloneCart[index].cartQty--
            if (cloneCart[index].cartQty === 0) {
                cloneCart.splice(index, 1);
            }
        }
        this.setState({ cart: cloneCart });
    }

    removeCartItem = (index) => {
        let cloneCart = [...this.state.cart];
        cloneCart.splice(index, 1);
        this.setState({ cart: cloneCart })
    }

    setStateModal = (item) => {
        this.setState({ productDetail: item })
    }

    render() {
        return (
            <div className='container'>
                <h2 style={{ fontSize: '50px' }}>Shoe Shop</h2>
                <Cart cart={this.state.cart} updateCart={this.updateCart} />
                <ProductList
                    productData={this.state.list}
                    addToCart={this.addToCart}
                    setStateModal={this.setStateModal} />
                <Modal body={this.state.productDetail} />
            </div>
        )
    }
}
